import pandas as pd
from datetime import datetime, timedelta

list_of_holidays = ["01.01.2021", "01.05.2021", "24.05.2021", "24.12.2021"]
list_of_holidays_dt = [datetime.strptime(date, '%d.%m.%Y') for date in list_of_holidays]

from katczinsky.main import calculate_hours_two_day_span, summarize_hours_over_month, monthly_account



def test_calculate_hours_may1():
    df, dummy =  calculate_hours_two_day_span("01.05.2021", 8)
    pd.testing.assert_frame_equal(
    df,
    pd.DataFrame({
        'hrs_bd'         : [6.0, 4.5],
        'hrs_bdnacht'    : [3.0, 6],
        'hrs_d'          : [5.17, 0],
        'hrs_dnacht'     : [0.0, 0],
        'is_holiday'     : [1.0,0],
        'is_sunday'      : [0.0,1]
    }, index = [datetime.strptime("01.05.2021", '%d.%m.%Y'),
             datetime.strptime("02.05.2021", '%d.%m.%Y')]))
    
def test_calculate_hours_may23():
    df, dummy =  calculate_hours_two_day_span("23.05.2021", 8)
    pd.testing.assert_frame_equal(
        df,
    pd.DataFrame({
        'hrs_bd'         : [6.0, 3.75],
        'hrs_bdnacht'    : [3.0, 6],
        'hrs_d'          : [5.17, 0],
        'hrs_dnacht'     : [0.0, 0],
        'is_holiday'     : [0,1.0],
        'is_sunday'      : [1.0,0]
    }, index = [datetime.strptime("23.05.2021", '%d.%m.%Y'),
             datetime.strptime("24.05.2021", '%d.%m.%Y')]))

def test_summarize_may():
    d1 = dict({'berdienst' : 52.75,
               'berdienst_nacht' : 26.0,
               'berdienst_feiertag' : 18.75,
               'dienst_sonntag' : 5.17,
               'dienst_feiertag' : 5.17,
               'dienst_nacht' : 8.0,
               'delta_dienst' : -2.49})
    assert d1 == summarize_hours_over_month(["01.05.2021", "07.05.2021", "23.05.2021", "26.05.2021"], 8)

def test_summarize_april():
    d1 = dict({'berdienst' : 13.50,
               'berdienst_nacht' : 9.0,
               'berdienst_feiertag' : 0.0,
               'dienst_sonntag' : 0.0,
               'dienst_feiertag' : 0.0,
               'dienst_nacht' : 7.0,
               'delta_dienst' : -13.66})
    assert d1 == summarize_hours_over_month(["14.04.2021", "29.04.2021"], 8)

        

def test_result_may():
    df =  monthly_account(["01.05.2021", "07.05.2021", "23.05.2021", "26.05.2021"], 8)
    pd.testing.assert_frame_equal(
        df,
    pd.DataFrame({
        'factor'      : [1, 1, 1, 1, 1.1, 1.1],
        'rate'        : [10.31, 14.43, 6.19, 5.4, 35.97, 8.99],
        'hrs'         : [5.17, 5.17, 8.00, 26.00, 52.75, 18.75],
        'total'       : [53.30, 74.60, 49.52,140.40, 2087.16, 185.42]
    }, index = ['JNN ZZ Sonntag',
                'JNN FeiertgZuschlag',
                'JNN Nachtarbeit-ZuSchl',
                'JNN ZZ Nachtarb 15%',
                'JLL BereitDienst',
                'JNN FZZ Ber.dienst'])
        )

def test_result_july():
    df =  monthly_account(["20.07.2021", "30.07.2021"], 8)
    pd.testing.assert_frame_equal(
        df,
    pd.DataFrame({
        'factor'      : [1, 1, 1, 1, 1.1, 1.1],
        'rate'        : [10.31, 14.43, 6.19, 5.4, 35.97, 8.99],
        'hrs'         : [0.0, 0.0, 8.00, 8.00, 14.50, 0.0],
        'total'       : [0.0, 0.0, 49.52, 43.20, 573.72, 0.0]
    }, index = ['JNN ZZ Sonntag',
                'JNN FeiertgZuschlag',
                'JNN Nachtarbeit-ZuSchl',
                'JNN ZZ Nachtarb 15%',
                'JLL BereitDienst',
                'JNN FZZ Ber.dienst'])
        )

